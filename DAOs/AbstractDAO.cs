﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAOs
{
    public abstract class AbstractDAO<TEntity> : IDisposable,
       InterfaceDAO<TEntity> where TEntity : class
    {
        private BancoDeDados banco = new BancoDeDados();

        public void Incluir(TEntity objeto)
        {
            this.banco.Set<TEntity>().Add(objeto);
        }

        public void Excluir(Func<TEntity, bool> predicate)
        {
           this.banco.Set<TEntity>().Where(predicate).ToList()
               .ForEach(del => banco.Set<TEntity>().Remove(del));
        }

        public void Atualizar(TEntity objeto)
        {
            this.banco.Entry(objeto).State = EntityState.Modified;
        }

        public void Salvar()
        {
            this.banco.SaveChanges();
        }

        public TEntity Carregar(params object[] key)
        {
            return this.banco.Set<TEntity>().Find(key);
        }

        public IQueryable<TEntity> Listar(Func<TEntity, bool> predicate)
        {
            return ListarTodos().Where(predicate).AsQueryable();
        }

        public IQueryable<TEntity> ListarTodos()
        {
            return this.banco.Set<TEntity>();
        }

       /**
        * Ao implementar Dispose, o objeto pode ser 
        * utilizado com using. Assim, quando for encontrado 
        * o fechamento das chaves do using, o objeto já 
        * é liberado e destruído da memória.
        **/
        public void Dispose()
        {
            this.banco.Dispose();
        }
    }
}