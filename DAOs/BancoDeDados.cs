﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Dominio;

namespace DAOs
{
    class BancoDeDados : DbContext
    {
        public BancoDeDados() : base("StrConexao") { }
        public DbSet<Pessoa> Pessoas { get; set; }
    }
}
