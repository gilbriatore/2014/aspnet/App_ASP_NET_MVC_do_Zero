﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAOs
{
    public interface InterfaceDAO<TEntity> where TEntity : class
    {
        void Incluir(TEntity objeto);
        void Excluir(Func<TEntity, bool> predicate);
        void Atualizar(TEntity objeto);
        void Salvar();
        TEntity Carregar(params object[] key); 
        IQueryable<TEntity> Listar(Func<TEntity, bool> predicate);
        IQueryable<TEntity> ListarTodos();
    }
}
