﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dominio;
using DAOs;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private PessoaDAO dao = new PessoaDAO();
       
        public ActionResult Index()
        {
            List<Pessoa> pessoas = dao.ListarTodos().ToList();
            return View(pessoas);
        }
        
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pessoa pessoa = dao.Carregar(id);
            if (pessoa == null)
            {
                return HttpNotFound();
            }
            return View(pessoa);
        }
        
        public ActionResult Create()
        {
            return View();
        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="PessoaID,Nome,Sobrenome,CPF")] Pessoa pessoa)
        {
            if (ModelState.IsValid)
            {
                dao.Incluir(pessoa);
                dao.Salvar();
                return RedirectToAction("Index");
            }
            return View(pessoa);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pessoa pessoa = dao.Carregar(id);
            if (pessoa == null)
            {
                return HttpNotFound();
            }
            return View(pessoa);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="PessoaID,Nome,Sobrenome,CPF")] Pessoa pessoa)
        {
            if (ModelState.IsValid)
            {
                dao.Atualizar(pessoa);
                dao.Salvar();
                return RedirectToAction("Index");
            }
            return View(pessoa);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Pessoa pessoa = dao.Carregar(id);
            if (pessoa == null)
            {
                return HttpNotFound();
            }
            return View(pessoa);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            dao.Excluir(p => p.PessoaID == id);
            dao.Salvar();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dao.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
