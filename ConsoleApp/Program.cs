﻿using DAOs;
using Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Pessoa p = new Pessoa();
            p.Nome = "João";
            p.Sobrenome = "Silva";
            p.CPF = "12345";

            PessoaDAO dao = new PessoaDAO();
            dao.Incluir(p);

        }
    }
}
